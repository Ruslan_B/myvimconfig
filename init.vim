call plug#begin()
Plug 'roxma/nvim-completion-manager'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/syntastic'
Plug 'valloric/youcompleteme'
Plug 'tpope/vim-surround'
Plug 'kien/ctrlp.vim'
Plug 'nvie/vim-flake8'
Plug 'prettier/vim-prettier'
Plug 'othree/html5.vim'
Plug 'tpope/vim-cucumber'
Plug 'stephenmckinney/vim-autotag'
Plug 'elixir-lang/vim-elixir'
call plug#end()

let g:python_host_prog = '~/venvs/neovim2/bin/python'
let g:python3_host_prog = '~/venvs/neovim3/bin/python'

set nu
set backspace=2 " make backspace work like most other programs
set nocompatible              " required


set splitbelow
set splitright

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" outo open NERD Tree
" autocmd VimEnter * NERDTree
map <C-n> :NERDTreeToggle<CR>
let NERDTreeShowHidden=1

" make your code look prettyt
let python_highlight_all=1
syntax on

" Enable folding
set foldmethod=indent
set foldlevel=99
" Enable folding with the spacebar
nnoremap <space> za

" Config for python files
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

" Config for js and web development
au BufNewFile,BufRead *.js,*.html,*.css
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4
 
" Config for Gherkin files
au BufNewFile,BufRead *.feature
       \ set tabstop=4 |
       \ set softtabstop=4 |
       \ set shiftwidth=4
"autocomplete tweek
au BufNewFile,BufRead *.sh
       \ set tabstop=4 |
       \ set softtabstop=4 |
       \ set shiftwidth=4
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
